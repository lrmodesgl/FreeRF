#ifndef _MAIN_HH_
#define _MAIN_HH_

#define DLLEXPORT extern "C"

#include <stdio.h>

#include "common.h"

DLLEXPORT int ReduceAngle(double angle);
DLLEXPORT double LonDiff(double lon1, double lon2);
DLLEXPORT int PutMask(double lat, double lon, int value);
DLLEXPORT int OrMask(double lat, double lon, int value);
DLLEXPORT int GetMask(double lat, double lon);
DLLEXPORT int PutSignal(double lat, double lon, unsigned char signal);
DLLEXPORT unsigned char GetSignal(double lat, double lon);
DLLEXPORT double GetElevation(struct site location);
DLLEXPORT int AddElevation(double lat, double lon, double height);
DLLEXPORT double Distance(struct site site1, struct site site2);
DLLEXPORT double Azimuth(struct site source, struct site destination);
DLLEXPORT double ElevationAngle(struct site source, struct site destination);
DLLEXPORT void ReadPath(struct site source, struct site destination);
DLLEXPORT double ElevationAngle2(struct site source, struct site destination, double er);
DLLEXPORT double ReadBearing(char *input);
DLLEXPORT void ObstructionAnalysis(struct site xmtr, struct site rcvr, double f,
			 FILE *outfile);

void free_elev(void);
void free_path(void);
void alloc_elev(void);
void alloc_path(void);
void do_allocs(void);

#endif /* _MAIN_HH_ */
