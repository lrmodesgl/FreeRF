import ctypes

# load shared lib
rflib = ctypes.cdll.LoadLibrary('lib/libcloudrf.so')

# call a function
op = rflib.ReduceAngle(ctypes.c_double(89.65))

print('Reduce Angle : {}'.format(op))

