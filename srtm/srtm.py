import os
from os import path
import numpy as np
import math

from scipy.misc import imresize, imsave
from matplotlib import pyplot as plt
# set plot style
plt.style.use('ggplot')


''' return the list of mini tiles in given directory '''
def get_cells(dir_):
    files = [f for f in os.listdir(dir_) if f.endswith('.hgt')]
    cells = [(int(item[1:3]),int(item[4:7])) for item in files]
    return cells

''' convert coordinates to filename '''
def coor2filename(n,e):
    return 'N{0}E{1}.hgt'.format(str(n).zfill(2),str(e).zfill(3))

''' get a mini tile from (.hgt) file '''
def get_mini_tile(fn):    
    siz = os.path.getsize(fn)
    dim = int(math.sqrt(siz/2))
    assert dim*dim*2 == siz, 'Invalid file size'
    return np.fromfile(fn, np.dtype('>i2'), dim*dim).reshape((dim, dim))

''' return a list of grid cell coordinates '''
def get_grid(cells):
    num_cells = len(cells)
    ncells, ecells = [], []
    for item in cells:
        ncells.append(item[0])
        ecells.append(item[1])
    nmin, nmax, emin, emax = min(ncells), max(ncells), min(ecells), max(ecells)
    # form grid
    columns = len(set(ecells))
    rows = math.floor(num_cells/columns)
    return rows, columns, nmin, nmax, emin, emax

''' does cell exist '''
def doesCellExist(cells,x,y):
    for item in cells:
        if x == item[0] and y == item[1]:
            return True
    return False

''' input : directory name; output : numpy tile '''
def get_tile(dir_,log=False):
    # get cells
    cells = get_cells(dir_)
    # get grid
    rows, columns, n_off, nmax, e_off, emax = get_grid(cells)
    # create a large tile of zeros
    tile = np.zeros([1201*(rows), 1201*(columns)])
    # iterate through grid cells
    for i in range(nmax - n_off-1, -1,-1 ):
        for j in range(emax - e_off):
            coor = (n_off+i, e_off+j)
            if doesCellExist(cells,n_off+1, e_off+j):
                if log:
                    print(coor, end='')
                d = get_mini_tile(dir_ + '/' + coor2filename(*coor))
                dim = d.shape[0]
                i_id = nmax - n_off -i -1
                tile[ i_id*dim:(i_id*dim)+dim, j*dim:(j*dim)+dim, ] = d
            else:
                if log:
                    print('xx-xx', end='')
        if log:
            print('')

    return tile

''' threshold image '''
def thresh(image):
    idx = image< 5
    image[idx] = 255
    return image

''' show tile in pyplot '''
def show_tile(tile,cmap='gray'):
    tile_res = imresize(tile, 25)
    plt.imshow(tile_res, cmap=cmap)
    plt.show()

''' get labels and coordinates from filename '''
def get_coordinates(filename):
    y_label, x_label = re.findall('[A-Z]+',filename)
    y,x = re.findall('\d+',filename)
    return y_label, x_label, int(y), int(x)



''' MAIN '''
if __name__ == '__main__':
    # convert list of .hgt files into a whole tile 
    tile = get_tile('D44') # folder D44/
    # threshold tile : just for visualization
    tile_th = thresh(tile)
    # display tile
    show_tile(tile_th, cmap=None)
    # save to file
    imsave('outfile.png', imresize(tile,25))
    # imsave('outfile.tiff', tile) # do no resize when using the data for next step
